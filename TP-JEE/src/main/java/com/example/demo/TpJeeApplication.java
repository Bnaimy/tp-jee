package com.example.demo;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import entities.Patient;
import repositories.PatientRepository;
@SpringBootApplication
@EntityScan("entities")
@EnableJpaRepositories("repositories")
public class TpJeeApplication implements CommandLineRunner {
	
	@Autowired
	PatientRepository pr ;
	

	public static void main(String[] args) {
		SpringApplication.run(TpJeeApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		pr.save(new Patient(1,"bouchaib",new Date(),1000,false));
		pr.save(new Patient(2,"mouad",new Date(),100,true));
		pr.save(new Patient(3,"lwejdi",new Date(),10,false));
		pr.findAll().forEach(p-> {
			System.out.println(p.toString());
		}
		);
		System.out.println("--------------------------------------------");
		Patient P = new Patient(1,"bouchaib",new Date(),1000,false);
		P.setNom("choaib");
		P.setScore(2000);
		P.setMalade(false);
		P.setDate_naissance(new Date());
		pr.save(P);
		pr.findAll().forEach(p-> {
			System.out.println(p.toString());
		}
		);
		System.out.println("-------------------------------------------------");
		pr.findById(3);
		pr.deleteById(2);
		pr.findAll().forEach(p-> {
			System.out.println(p.toString());
		}
		);
		System.out.println("-------------------------------------------------");
		pr.findByMalade(false).forEach(
			patient->{
				System.out.println(patient.toString());
			}
		);
	}
	
	

}
