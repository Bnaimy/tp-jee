package repositories;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import entities.Patient;


public interface PatientRepository extends JpaRepository<Patient, Integer> {
	
	 List<Patient>  findByMalade(boolean maladie);
}
